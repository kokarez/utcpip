#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <linux/udp.h>
#include <linux/if_ether.h>
#include <arpa/inet.h>

#include "tcpip.h"
#include "udp.h"
#include "upkt.h"

static udp_pcb *udp_pcbs = NULL;

static void insert_pcb(udp_pcb *pcb) {
  udp_pcb *tmp;

  if (pcb->next)
    return;

  for (tmp = udp_pcbs; tmp; tmp = tmp->next)
     if (tmp == pcb)
        return;

  if (udp_pcbs)
    pcb->next = udp_pcbs;
  udp_pcbs = pcb;
}

static udp_pcb *search_pcb_by_port(uint16_t port) {
  udp_pcb *temp;

  for (temp = udp_pcbs; temp; temp = temp->next)
     if (temp->local_port == port)
        break;

  return temp;
}

static void remove_pcb(udp_pcb *pcb) {
  udp_pcb *prev = udp_pcbs;
  udp_pcb *temp;

  if (udp_pcbs == NULL)
    return;

  if (udp_pcbs == pcb) {
    udp_pcbs = udp_pcbs->next;
    pcb->next = NULL;
    return;
  }

  temp = udp_pcbs->next;

  while (temp) {
    if (temp == pcb) {
      prev->next = temp->next;
      pcb->next = NULL;
      return;
    }
    prev = temp;
    temp = temp->next;

  }
}

static uint16_t udp_checksum(uint16_t *buf,
                             uint32_t len,
                             uint32_t ipsrc,
                             uint32_t ipdst) {
  uint32_t  tmp_len = len;
  uint32_t  sum = 0;
  uint16_t *ip;

  while (tmp_len > 1) {
    sum += *buf++;
    if (sum & 0x80000000)
      sum = (sum & 0xFFFF) + (sum >> 16);
    tmp_len -= 2;
  }

  if (tmp_len & 1)
    sum += *((uint8_t *)buf);

  ip = (void *)&ipsrc;
  sum += *(ip++);
  sum += *(ip);

  ip = (void *)&ipdst;
  sum += *(ip++);
  sum += *(ip);

  sum += htons(IPPROTO_UDP);
  sum += htons(len);

  while (sum >> 16)
    sum = (sum & 0xFFFF) + (sum >> 16);

  return (uint16_t)(~sum);
}

udp_pcb *udp_new(void) {
  udp_pcb *pcd = malloc(sizeof(udp_pcb));

  if (pcd == NULL)
    return pcd;

  memset(pcd, 0, sizeof(udp_pcb));
  return pcd;
}

void udp_rel(udp_pcb *pcb) {
  if (pcb->next) {
    // Need to disconneect
    remove_pcb(pcb);
  }
  free(pcb);
}

void udp_bind(udp_pcb *pcb, uint32_t ip, uint16_t port) {
  pcb->local_ip = ip;
  pcb->local_port = port;
  pcb->flags |= UDP_PCB_F_BINDED;
  insert_pcb(pcb);
}

void udp_connect(udp_pcb *pcb, uint32_t ip, uint16_t port) {
  pcb->remote_ip = ip;
  pcb->remote_port = port;
  pcb->flags |= UDP_PCB_F_CONNECTED;
  insert_pcb(pcb);
}

int udp_sendto(udp_pcb     *pcb,
               useless_pkt *upkt,
               uint32_t     ip,
               uint16_t     port) {
  struct udphdr *udp = upkt_header(upkt, UPKT_L_TRANSPORT);

  uint32_t ip_dst = 0xffffffff;

  // I suppose we should support multiple interface :D but no
  udp->source = htons(pcb->local_port);
  udp->dest = htons(port);
  udp->len = htons(upkt->size - (UPKT_LINK_HLEN + UPKT_IP_HLEN));
  udp->check = udp_checksum((void *)udp, ntohs(udp->len), pcb->local_ip, ip_dst);

  ip_output(upkt, pcb->uif, pcb->local_ip, ip_dst, IPPROTO_UDP);

  return 0;
}

int  udp_send(udp_pcb *pcb, useless_pkt *upkt) {
  return udp_sendto(pcb, upkt, pcb->remote_ip, pcb->remote_port);
}

void udp_input(useless_pkt* upkt, uiface *uif) {
  struct udphdr  *udp = upkt->data;
  struct udp_pcb *pcb;

  // Dropbox flewed so let block for the moment
  if (htons(udp->dest) == 17500)
    goto free_upkt;

  pcb = search_pcb_by_port(htons(udp->dest));
  if (pcb == NULL) {
    goto free_upkt;
  }

  if (!pcb->recv) {
    printf("recv callback unset\n");
    goto free_upkt;
  }

  pcb->recv(pcb, upkt);
free_upkt:
  upkt_free(upkt);
}
