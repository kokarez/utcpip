#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>

uint64_t time_ms() {
  struct timeval te;

  gettimeofday(&te, NULL);

  return (te.tv_sec * 1000LL) + (te.tv_usec / 1000);
}

uint8_t *mactostr(uint8_t *mac) {
  static uint8_t output[18] = { 0 };
  const int8_t   hexa[] = "0123456789abcdef";

  for (uint8_t index = 2; index < 18; index += 3)
    output[index] = ':';

  for (uint8_t index = 0; index < 6; index += 1) {
    output[(index * 3)]     = hexa[(mac[index] & 0xF0) >> 4];
    output[(index * 3) + 1] = hexa[(mac[index] & 0x0F)];
  }

  output[17] = '\0';
  return (uint8_t *)output;
}
