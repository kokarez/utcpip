#ifndef UDP_H
#define UDP_H

#include <stdint.h>

#include "uiface.h"
#include "upkt.h"

struct udp_pcb;
typedef void (*udp_recv_fn)(struct udp_pcb *pcb, useless_pkt *pkt);

#define UDP_PCB_F_BINDED    0x1
#define UDP_PCB_F_CONNECTED 0x2

typedef struct udp_pcb {
  uint32_t        local_ip;
  uint32_t        remote_ip;
  uint16_t        local_port;
  uint16_t        remote_port;
  uint16_t        flags;
  struct udp_pcb *next;
  udp_recv_fn     recv;
  uiface         *uif;
  void           *private;
} udp_pcb;

/* struct management */
udp_pcb *udp_new(void);
void     udp_rel(udp_pcb *pcb);

/* connection management */
void     udp_bind(udp_pcb *pcb, uint32_t ip, uint16_t port);
void     udp_connect(udp_pcb *pcb, uint32_t ip, uint16_t port);

/* pkt management */
int      udp_sendto(udp_pcb *pcb, useless_pkt *upkt, uint32_t ip, uint16_t port);
int      udp_send(udp_pcb *pcb, useless_pkt *upkt);
void     udp_input(useless_pkt* upkt, uiface *uif);

#endif // !UDP_H
