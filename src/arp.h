#ifndef ARP_H
#define ARP_H

#include <linux/if_arp.h>
#include <stdint.h>

typedef struct {
  struct   arphdr arphdr;
  uint8_t  sender_mac[6];
  uint32_t sender_ip;
  uint8_t  target_mac[6];
  uint32_t target_ip;
} __attribute__((packed)) uarphdr;

#endif // !ARP_H
