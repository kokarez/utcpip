#ifndef TCPIP_H
#define TCPIP_H

#include "uiface.h"
#include "upkt.h"

void dump_stats(void);
void ether_input(useless_pkt *upkt, uiface *uif);
void ip_input(useless_pkt *upkt, uiface *uif);
void arp_input(useless_pkt *upkt, uiface *uif);
void rarp_input(useless_pkt *upkt, uiface *uif);

void ether_output(useless_pkt *upkt,
                  uiface *uif,
                  uint16_t ether_type,
                  char *dst);
void ip_output(useless_pkt *upkt,
               uiface *uif,
               uint32_t src_ip,
               uint32_t dst_ip,
               uint8_t proto);

uint64_t  time_ms(void);
uint8_t  *mactostr(uint8_t *mac);

#endif // !TCPIP_H
