#ifndef UIFACE_H
#define UIFACE_H

#define UIFACE_NAME_MAX 255

#include "upkt.h"

struct uiface;
typedef void (*uiface_output)(struct uiface *, useless_pkt *upkt);

typedef struct uiface {
  int           fd;
  uint32_t      ip;
  unsigned char mac[6];
  char          name[UIFACE_NAME_MAX];
  uiface_output output;
} uiface;

extern uiface *g_uif;

#endif // !UIFACE_H
