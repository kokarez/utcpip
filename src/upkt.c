#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "upkt.h"

/*
 * Here is how we allocate a upkt and format the structure
 *
 * for a request like UPKT_TRANSPORT_HLEN
 *
 * Allocation:
 * +------+-------+----+---------+-----------+
 * | upkt | ether | ip | UDP/TCP |  payload  |
 * +------+-------+----+---------+-----------+
 * |      |       |    |         |
 * |      |       |    |         \-> upkt->data UPKT_L_PAYLOAD
 * |      |       |    \--> upkt->data if UPKT_L_TRANSPORT
 * |      |       \-> upkt->data if UPKT_L_IP
 * |      \-> upkt->data if UPKT_L_ETH
 * \--> the upkt pointer
 *
 * you can move the `->data` with upkt_move.
 */

useless_pkt *upkt_alloc(upkt_layer layer, uint16_t len, void *payload) {
  useless_pkt *new_pkt;
  unsigned int offset;

  switch (layer) {
    case UPKT_L_TCP_PAYLOAD:
      offset = UPKT_LINK_HLEN + UPKT_IP_HLEN + UPKT_TCP_HLEN;
      break;
    case UPKT_L_UDP_PAYLOAD:
      offset = UPKT_LINK_HLEN + UPKT_IP_HLEN + UPKT_UDP_HLEN;
      break;
    case UPKT_L_TRANSPORT:
      offset = UPKT_LINK_HLEN + UPKT_IP_HLEN;
      break;
    case UPKT_L_IP:
    case UPKT_L_ARP:
      offset = UPKT_LINK_HLEN;
      break;
    case UPKT_L_ETH:
      offset = 0;
      break;
    default:
      printf("Wrong layer given in arg [%d]\n", layer);
      return NULL;
  }

  new_pkt = calloc(sizeof(useless_pkt) + offset + len, 1);
  new_pkt->data = ((char *)(new_pkt + 1)) + offset;
  new_pkt->size = offset + len;

  if (payload == NULL)
    return new_pkt;

  memcpy(new_pkt->data, payload, len);
  return new_pkt;
}

void *upkt_header(useless_pkt *upkt, upkt_layer layer) {
  uint16_t offset;
  void    *header;

  // Reset
  header = upkt + 1;

  switch (layer) {
    case UPKT_L_TCP_PAYLOAD:
      offset = UPKT_LINK_HLEN + UPKT_IP_HLEN + UPKT_TCP_HLEN;
      break;
    case UPKT_L_UDP_PAYLOAD:
      offset = UPKT_LINK_HLEN + UPKT_IP_HLEN + UPKT_UDP_HLEN;
      break;
    case UPKT_L_TRANSPORT:
      offset = UPKT_LINK_HLEN + UPKT_IP_HLEN;
      break;
    case UPKT_L_IP:
    case UPKT_L_ARP:
      offset = UPKT_LINK_HLEN;
      break;
    case UPKT_L_ETH:
      offset = 0;
      break;
    default:
      printf("Wrong layer given in arg [%d]\n", layer);
      return NULL;
  }

  return ((char *)header) + offset;
}

void upkt_free(useless_pkt *upkt) {
  free(upkt);
}

void upkt_move(useless_pkt *upkt, uint16_t len) {
  upkt->data += len;
}
