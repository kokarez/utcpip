#ifndef DHCP_H
#define DHCP_H

#include <linux/if_ether.h>
#include <stdint.h>

#include "upkt.h"
#include "uiface.h"

/*
 * https://www.ietf.org/rfc/rfc2131.txt
 */

#define DHCP_MSG_DISCOVER  1
#define DHCP_MSG_OFFER     2
#define DHCP_MSG_REQUEST   3
#define DHCP_MSG_DECLINE   4
#define DHCP_MSG_ACK       5
#define DHCP_MSG_NAK       6
#define DHCP_MSG_RELEASE   7
#define DHCP_MSG_INFORM    8

#define DHCP_HDR_HWADDR_LEN     16
#define DHCP_HDR_SNAME_LEN      64
#define DHCP_HDR_FILE_LEN       128
#define DHCP_HDR_MIN_OPTION_LEN 70   // Need to find the real value

#define DHCP_HDR_COOKIE         0x63538263

#define DHCP_CLIENT_PORT 68
#define DHCP_SERVER_PORT 67

struct dhcp_hdr {
   uint8_t  op;
   uint8_t  htype;
   uint8_t  hlen;
   uint8_t  hops;
   uint32_t xid;
   uint16_t secs;
   uint16_t flags;
   uint32_t ciaddr;
   uint32_t yiaddr;
   uint32_t siaddr;
   uint32_t giaddr;
   uint8_t  chaddr[DHCP_HDR_HWADDR_LEN];
   int8_t   sname[DHCP_HDR_SNAME_LEN];
   int8_t   file[DHCP_HDR_FILE_LEN];
   uint32_t cookies;                       // 99, 130, 83 and 99
   uint8_t  options[DHCP_HDR_MIN_OPTION_LEN];
};

/* https://www.ietf.org/rfc/rfc2132.txt */
#define DHCP_OPTION_PAD            0
#define DHCP_OPTION_SUBNET_MASK    1
#define DHCP_OPTION_ROUTER         3
#define DHCP_OPTION_DNS_SERVER     6
#define DHCP_OPTION_HOSTNAME       12
#define DHCP_OPTION_IP_TTL         23
#define DHCP_OPTION_MTU            26
#define DHCP_OPTION_BROADCAST      28
#define DHCP_OPTION_TCP_TTL        37
#define DHCP_OPTION_REQUESTED_IP   50
#define DHCP_OPTION_LEASE_TIME     51
#define DHCP_OPTION_OVERLOAD       52
#define DHCP_OPTION_MESSAGE_TYPE   53
#define DHCP_OPTION_SERVER_ID      54
#define DHCP_OPTION_PARAM_REQ_LIST 55
#define DHCP_OPTION_MAX_MSG_SIZE   57
#define DHCP_OPTION_T1             58
#define DHCP_OPTION_T2             59
#define DHCP_OPTION_US             60
#define DHCP_OPTION_CLIENT_ID      61
#define DHCP_OPTION_TFTP_SNAME     66
#define DHCP_OPTION_BOOTFILE       67
#define DHCP_OPTION_END            255

#define DHCP_OPTION_SERVER_ID_LEN    4
#define DHCP_OPTION_MESSAGE_TYPE_LEN 1
#define DHCP_OPTION_MAX_MSG_SIZE_LEN 2

typedef struct {
  uint32_t         xid;
  uint8_t          remaining;
  uint8_t         *options;
  uint16_t         options_pos;
  useless_pkt     *upkt;
  struct dhcp_hdr *hdr;
} dhcp_req;

#define DHCP_DISC_RETRY_TIME (5 * 1000)

#define DHCPP_STATE_NONE      0
#define DHCPP_STATE_OFFERED   1
#define DHCPP_STATE_ACKED     2

typedef struct {
  uint64_t last_disc;
  uint8_t  stats;
} dhcpp;

#define pcb_dhcpp(pcb) ((dhcpp *)(pcb->private))

void dhcp_init(uiface *uif);
void dhcp_update(uiface *uif);

#endif // !DHCP_H
