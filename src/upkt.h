#ifndef PKT_H
#define PKT_H

#include <stdint.h>

#define UPKT_F_BROADCAST 0x01

typedef struct {
  uint8_t  flags;
  uint32_t size;
  void    *ethhdr;
  void    *iphdr;
  void    *data;
} useless_pkt;

typedef enum {
  UPKT_L_TCP_PAYLOAD = 1,
  UPKT_L_UDP_PAYLOAD,
  UPKT_L_TRANSPORT,
  UPKT_L_IP,
  UPKT_L_ARP,
  UPKT_L_ETH
} upkt_layer;

#define UPKT_LINK_HLEN 14
#define UPKT_IP_HLEN   20
#define UPKT_TCP_HLEN  20
#define UPKT_UDP_HLEN  8

useless_pkt *upkt_alloc(upkt_layer layer, uint16_t len, void *data);
void         upkt_free(useless_pkt *upkt);
void         upkt_move(useless_pkt *upkt, uint16_t len);
void        *upkt_header(useless_pkt *upky, upkt_layer layer);

#endif // !PKT_H
