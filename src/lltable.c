#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/ip.h>
#include <linux/if.h>
#include <arpa/inet.h>
#include <string.h>
#include <linux/if_ether.h>

#include "tcpip.h"
#include "arp.h"
#include "uiface.h"
#include "lltable.h"

static llentry *head = NULL;

static llentry *llentry_find(uint32_t ip) {
  llentry *itr;

  for (itr = head; itr; itr = itr->next)
    if (itr->dst_ip == ip)
      return itr;

  return NULL;
}

llentry *lltable_insert(uint32_t ip, uint8_t *mac) {
  llentry *entry = llentry_find(ip);

  if (!entry) {
    entry = malloc(sizeof (llentry));
    if (!entry)
      return NULL;
    entry->dst_ip = ip;
    entry->flags = 0;
    entry->next = head;
    head = entry;
  }

  memcpy(entry->dst_mac, mac, 6);
  return entry;
}

void arp_request(uiface *uif, uint32_t ip) {
  useless_pkt *upkt;
  uarphdr     *arp;


  upkt = upkt_alloc(UPKT_LINK_HLEN, sizeof (uarphdr), NULL);
  arp = upkt_header(upkt, UPKT_L_ARP);

  arp->arphdr.ar_hrd = htons(ARPHRD_ETHER);
  arp->arphdr.ar_pro = htons(0x0800);
  arp->arphdr.ar_hln = 6;
  arp->arphdr.ar_pln = 4;
  arp->arphdr.ar_op  = htons(1); // request

  memcpy(arp->sender_mac, uif->mac, 6);
  arp->sender_ip = htonl(uif->ip);
  memcpy(arp->target_mac, "\x00\x00\x00\x00\x00\x00", 6);
  arp->target_ip = htonl(ip);

  ether_output(upkt, uif, ETH_P_ARP, "\xff\xff\xff\xff\xff\xff");

  upkt_free(upkt);
}

llentry *lltable_get(uint32_t ip) {
  llentry *entry = llentry_find(ip);

  if (entry)
    return entry;

  entry = lltable_insert(ip, (uint8_t *)"\x00\x00\x00\x00\x00\x00");
  if (!entry)
    return NULL;

  entry->flags = LLE_F_PENDING;
  arp_request(g_uif, ip);
  return entry;
}

void lltable_dump() {
  llentry *itr;

  printf("|       mac        |   ip\n");
  for (itr = head; itr; itr = itr->next) {
    struct in_addr ipv4 = { .s_addr = htonl(itr->dst_ip) };
    printf("| %s | %s\n", mactostr(itr->dst_mac), inet_ntoa(ipv4));
  }

}
