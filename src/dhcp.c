#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/ioctl.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <linux/if.h>

#include "tcpip.h"
#include "dhcp.h"
#include "udp.h"
#include "upkt.h"

#define log(fmt, ...) printf(__TIME__ ": dhcp: " fmt "\n", ##__VA_ARGS__);

static udp_pcb *pcb = NULL;

static void dhcp_option_start(dhcp_req *req, uint8_t type, uint8_t size) {
  req->options[req->options_pos++] = type;
  req->options[req->options_pos++] = size;
  req->remaining = size;
}

static void dhcp_option_pushb(dhcp_req *req, uint8_t value) {
  req->options[req->options_pos++] = value;
  req->remaining -= sizeof (uint8_t);
}

static void dhcp_option_pushs(dhcp_req *req, uint16_t value) {
  req->options[req->options_pos++] = (uint8_t)((value & 0xFF00) >> 8);
  req->options[req->options_pos++] = (uint8_t)((value & 0x00FF));
  req->remaining -= sizeof (uint16_t);
}

static void dhcp_option_push(dhcp_req *req, uint32_t value) {
  req->options[req->options_pos++] = (uint8_t)((value & 0xFF000000) >> 24);
  req->options[req->options_pos++] = (uint8_t)((value & 0x00FF0000) >> 16);
  req->options[req->options_pos++] = (uint8_t)((value & 0x0000FF00) >> 8);
  req->options[req->options_pos++] = (uint8_t)((value & 0x000000FF));
  req->remaining -= sizeof (uint16_t);
}

static void dhcp_option_fini(dhcp_req *req) {
  req->options[req->options_pos++] = DHCP_OPTION_END;
}

static void *dhcp_option_get(struct dhcp_hdr *dhcp, uint8_t type, uint8_t *size) {
  uint8_t  cur_type = 0;
  uint8_t *options = dhcp->options;

  while (cur_type != DHCP_OPTION_END) {
    cur_type = *options++;
    *size    = *options++;

    if (cur_type == 0)
      return NULL;

    if (cur_type == type)
      return options;

    options += *size;
  }

  return NULL;
}

static int dhcp_create_req(dhcp_req *req, uint8_t type) {
  static int xid = 0xDEADBEEF;

  memset(req, 0, sizeof(dhcp_req));

  req->upkt = upkt_alloc(UPKT_L_UDP_PAYLOAD, sizeof (struct dhcp_hdr), NULL);
  if (req->upkt == NULL) {
    return 1;
  }

  req->hdr = req->upkt->data;
  req->options = req->hdr->options;

  req->hdr->op = 1; /* BOOTREQUEST */
  req->hdr->htype = 1; /* '1' for 10mb */
  req->hdr->hlen = 6; /* '6' for 10mb */
  req->hdr->hops = 0;
  req->hdr->secs = 0;
  req->hdr->xid = htonl(xid++);

  req->hdr->flags = (1 << 7); // We need to broadcast

  req->hdr->ciaddr = 0;
  if (0 /* if we already an addr */) {
  }

  req->hdr->yiaddr = 0;
  req->hdr->siaddr = 0;
  req->hdr->giaddr = 0;

  memcpy(req->hdr->chaddr, pcb->uif->mac, 6);

  memset(req->hdr->sname, 0, DHCP_HDR_SNAME_LEN);
  memset(req->hdr->file, 0, DHCP_HDR_FILE_LEN);

  req->hdr->cookies = DHCP_HDR_COOKIE;

  for (int index = 0; index < DHCP_HDR_MIN_OPTION_LEN; index++)
     req->hdr->options[index] = '\xA5';

  dhcp_option_start(req, DHCP_OPTION_MESSAGE_TYPE, DHCP_OPTION_MESSAGE_TYPE_LEN);
  dhcp_option_pushb(req, type);

  return 0;
}

static void dhcp_release_req(dhcp_req *req) {
  upkt_free(req->upkt);
}

static void dhcp_request(uint32_t requested_ip, uint32_t server_id) {
  dhcp_req req;

  if (dhcp_create_req(&req, DHCP_MSG_REQUEST)) {
    log("Unable to process the discovery: no memory");
  }

  dhcp_option_start(&req, DHCP_OPTION_REQUESTED_IP, sizeof (uint32_t));
  dhcp_option_push(&req, requested_ip);

  dhcp_option_start(&req, DHCP_OPTION_SERVER_ID, sizeof (uint32_t));
  dhcp_option_push(&req, server_id);

  dhcp_option_start(&req, DHCP_OPTION_MAX_MSG_SIZE, DHCP_OPTION_MAX_MSG_SIZE_LEN);
  dhcp_option_pushs(&req, 1500 /* need to think about that */);

  dhcp_option_start(&req, DHCP_OPTION_PARAM_REQ_LIST, 4);
  dhcp_option_pushb(&req, DHCP_OPTION_SUBNET_MASK);
  dhcp_option_pushb(&req, DHCP_OPTION_ROUTER);
  dhcp_option_pushb(&req, DHCP_OPTION_BROADCAST);
  dhcp_option_pushb(&req, DHCP_OPTION_DNS_SERVER);

  dhcp_option_fini(&req);

  req.upkt->flags |= UPKT_F_BROADCAST;

  udp_send(pcb, req.upkt);

  dhcp_release_req(&req);
}

static void dhcp_discovery(uiface *uif) {
  dhcp_req req;

  if (time_ms() - pcb_dhcpp(pcb)->last_disc < DHCP_DISC_RETRY_TIME)
     return;

  pcb_dhcpp(pcb)->last_disc = time_ms();

  if (dhcp_create_req(&req, DHCP_MSG_DISCOVER)) {
    log("Unable to process the discovery: no memory");
  }

  dhcp_option_start(&req, DHCP_OPTION_MAX_MSG_SIZE, DHCP_OPTION_MAX_MSG_SIZE_LEN);
  dhcp_option_pushs(&req, 1500 /* need to think about that */);

  dhcp_option_start(&req, DHCP_OPTION_PARAM_REQ_LIST, 4);
  dhcp_option_pushb(&req, DHCP_OPTION_SUBNET_MASK);
  dhcp_option_pushb(&req, DHCP_OPTION_ROUTER);
  dhcp_option_pushb(&req, DHCP_OPTION_BROADCAST);
  dhcp_option_pushb(&req, DHCP_OPTION_DNS_SERVER);

  dhcp_option_fini(&req);

  req.upkt->flags |= UPKT_F_BROADCAST;

  udp_send(pcb, req.upkt);

  dhcp_release_req(&req);
}

static void dhcp_ack(udp_pcb *pcb, struct dhcp_hdr *dhcp) {
  uint8_t         *option;
  uint8_t          len;
  uint32_t         ip, netmask, router, brdcast, dns1, dns2;

  if (pcb_dhcpp(pcb)->stats >= DHCPP_STATE_ACKED)
    return;

  pcb_dhcpp(pcb)->stats = DHCPP_STATE_ACKED;

  ip = 0;
  netmask = 0;
  router = 0;
  brdcast = 0;
  dns1 = 0;
  dns2 = 0;

  /* Yea we have an IP */
  ip = ntohl(dhcp->yiaddr);

  option = dhcp_option_get(dhcp, DHCP_OPTION_SUBNET_MASK, &len);
  if (option)
    netmask = ntohl(*(uint32_t*)option);

  option = dhcp_option_get(dhcp, DHCP_OPTION_ROUTER, &len);
  if (option)
    router = ntohl(*(uint32_t*)option);

  option = dhcp_option_get(dhcp, DHCP_OPTION_BROADCAST, &len);
  if (option)
    brdcast = ntohl(*(uint32_t*)option);

  option = dhcp_option_get(dhcp, DHCP_OPTION_DNS_SERVER, &len);
  if (option) {
    len /= sizeof (uint32_t);
    dns1 = ntohl(*(uint32_t*)option);
    option += 4;
    if (len > 1)
      dns2 = ntohl(*(uint32_t*)option);
  }

#define _print_ip(_ip)                  \
  {                                     \
    struct in_addr ipv4;                \
    ipv4.s_addr = htonl(_ip);           \
    log( #_ip ": %s", inet_ntoa(ipv4)); \
  }

  _print_ip(ip);
  _print_ip(netmask);
  _print_ip(router);
  _print_ip(brdcast);
  _print_ip(dns1);
  _print_ip(dns2);

#undef _print_ip

  {
    // We need to create a separate socket to set the ip

    struct ifreq        ifr;
    int                 ctrl_fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
    struct sockaddr_in *addr = (struct sockaddr_in*)&ifr.ifr_addr;

    strncpy(ifr.ifr_name, pcb->uif->name, IFNAMSIZ);
    ifr.ifr_addr.sa_family = AF_INET;

    addr->sin_addr.s_addr = htonl(ip);
    if (ioctl(ctrl_fd, SIOCSIFADDR, &ifr) < 0) {
      /* Need to release the IP */
      log("didn't succeed to set the ip: %s", strerror(errno));
    }

    addr->sin_addr.s_addr = htonl(netmask);
    if (ioctl(ctrl_fd, SIOCSIFNETMASK, &ifr) < 0) {
      /* Need to release the IP */
      log("didn't succeed to set the netmask: %s", strerror(errno));
    }
  }
}

static void dhcp_offer(udp_pcb *pcb, struct dhcp_hdr *dhcp) {
  uint8_t  *option;
  uint8_t   len;
  uint32_t  requested_ip;
  uint32_t  server_id;

  if (pcb_dhcpp(pcb)->stats >= DHCPP_STATE_OFFERED)
    return;


  pcb_dhcpp(pcb)->stats = DHCPP_STATE_OFFERED;

  requested_ip = ntohl(dhcp->yiaddr);
  option = dhcp_option_get(dhcp, DHCP_OPTION_SERVER_ID, &len);
  server_id = ntohl(*(uint32_t*)option);

  dhcp_request(requested_ip, server_id);
}

static void dhcp_recv(struct udp_pcb *pcb, useless_pkt *upkt) {
  struct dhcp_hdr *dhcp = upkt_header(upkt, UPKT_L_UDP_PAYLOAD);
  uint8_t         *option;
  uint8_t          len;

  if (dhcp->cookies != DHCP_HDR_COOKIE) {
    log("msg without the correct cookie");
    return;
  }

  option = dhcp_option_get(dhcp, DHCP_OPTION_MESSAGE_TYPE, &len);

  switch (*option) {
    case DHCP_MSG_OFFER:
      dhcp_offer(pcb, dhcp);
      break;
    case DHCP_MSG_ACK:
      dhcp_ack(pcb, dhcp);
      break;
    default:
      log("Received unknow msg type: %d", *option);
      break;
  }
}

void dhcp_init(uiface *uif) {
  log("Starting dhcp on %s", uif->name);
  if (pcb != NULL) {
    log("DHCP double init");
    return;
  }

  pcb = udp_new();

  pcb->recv = dhcp_recv;
  pcb->uif = uif;
  pcb->private = calloc(sizeof (dhcpp), 1);

  pcb_dhcpp(pcb)->stats = DHCPP_STATE_NONE;

  // Delay the first disc
  pcb_dhcpp(pcb)->last_disc = time_ms() - (3 * 1000);

  udp_bind(pcb, htonl(INADDR_ANY), DHCP_CLIENT_PORT);
  udp_connect(pcb, htonl(INADDR_ANY), DHCP_SERVER_PORT);

}

void dhcp_update(uiface *uif) {
  switch (pcb_dhcpp(pcb)->stats) {
    case DHCPP_STATE_NONE:
      dhcp_discovery(uif);
      break;
    default:
      break;
  }
  return;
}
