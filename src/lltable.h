#ifndef LLTABLE_H_
#define LLTABLE_H_

#include <stdint.h>

#define LLE_F_PENDING (1 << 0)

typedef struct llentry {
  uint8_t        flags;
  uint32_t       dst_ip;
  uint8_t        dst_mac[6];
  struct llentry *next;
} llentry;

llentry *lltable_insert(uint32_t ip, uint8_t mac[6]);
llentry *lltable_get(uint32_t ip);
void     lltable_dump(void);

#endif /* LLTABLE_H_ */
