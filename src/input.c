#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <netinet/ip.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <arpa/inet.h>

#include "udp.h"
#include "upkt.h"
#include "tcpip.h"
#include "arp.h"
#include "lltable.h"

typedef struct {
   int ip;
   int arp;
   int rarp;
} utcpip_stats;

static utcpip_stats ustats = {
   .ip = 0,
   .arp = 0,
   .rarp = 0
};

void dump_stats() {
  printf("Ip[%d] Arp[%d] Rarp[%d]\n", ustats.ip, ustats.arp, ustats.rarp);
}

void ether_input(useless_pkt *upkt, uiface *uif) {
  struct ethhdr *ethhdr;

  ethhdr = (struct ethhdr *)upkt->data;

  if (upkt->size < 20) {
    printf("Invalid size: %d\n", upkt->size);
    goto free_pkt;
  }

  // Filter the pkt which are not for us (mac filter)
  if (memcmp(ethhdr->h_dest, "\xff\xff\xff\xff\xff\xff", 6) != 0) {
    if (memcmp(ethhdr->h_dest, uif->mac, 6) != 0) {
       return;
    }
  }

  upkt->ethhdr = ethhdr;

  upkt_move(upkt, UPKT_LINK_HLEN);
  switch (htons(ethhdr->h_proto)) {
    case ETH_P_IP:
      ustats.ip++;
      ip_input(upkt, uif);
      break;
    case ETH_P_ARP:
      ustats.arp++;
      arp_input(upkt, uif);
      break;
    case ETH_P_RARP:
      ustats.rarp++;
      rarp_input(upkt, uif);
      break;
    case ETH_P_IPV6:
      // Let's be honnest, I will never support ipv6
      goto free_pkt;
    default:
      printf("Unsupported proto: %X\n", htons(ethhdr->h_proto));
      goto free_pkt;
  }

  return;
free_pkt:
  upkt_free(upkt);
}

void ip_input(useless_pkt *upkt, uiface *uif) {
  struct iphdr *ip = upkt->data;

  // Need to check if the pkt is for use

  upkt->iphdr = ip;
  upkt_move(upkt, ip->ihl * 4);

  switch (ip->protocol) {
    case IPPROTO_UDP:
      udp_input(upkt, uif);
      return;
    default:
      break;
  }

  upkt_free(upkt);
}

void arp_input(useless_pkt *upkt, uiface *uif) {
  uarphdr       *arp = upkt_header(upkt, UPKT_L_ARP);

  /* 1) check if the arp table have this ip */

  lltable_insert(ntohl(arp->sender_ip), arp->sender_mac);

  /* 2.a) check if this request is for us */
  /* 2.b) check if this reply is for us */

  lltable_dump();

  upkt_free(upkt);
}

void rarp_input(useless_pkt *upkt, uiface *uif) {
  upkt_free(upkt);
}
