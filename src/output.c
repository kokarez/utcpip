#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <linux/ip.h>
#include <linux/if_ether.h>

#include "tcpip.h"

void ether_output(useless_pkt *upkt, uiface *uif, uint16_t proto, char *dst) {
  struct ethhdr *ethhdr;

  ethhdr = upkt_header(upkt, UPKT_L_ETH);

  memcpy(ethhdr->h_source, uif->mac, 6);
  memcpy(ethhdr->h_dest, dst, 6);
  ethhdr->h_proto = htons(proto);

  if (uif->output == NULL) {
    printf("Interface %s don't have any output func\n", uif->name);
    return;
  }

  uif->output(uif, upkt);
}

static uint16_t ip_checksum(void *buf, uint32_t len) {
  uint64_t  sum = 0;
  uint16_t *p = buf;

  while (len > 1) {
    sum += *p++;
    if (sum & 0x80000000)
      sum = (sum & 0xFFFF) + (sum >> 16);
    len -= 2;
  }

  while (sum >> 16)
    sum = (sum & 0xFFFF) + (sum >> 16);

  return(~sum);
}

void ip_output(useless_pkt *upkt,
               uiface *uif,
               uint32_t src_ip,
               uint32_t dst_ip,
               uint8_t proto) {
  struct iphdr *ip = upkt_header(upkt, UPKT_L_IP);
  char         *dst;

  ip->version = 4;
  ip->protocol = proto;
  ip->id = htons(0xBEEF);
  ip->frag_off = htons(0x4000);
  ip->tot_len = htons(upkt->size - UPKT_LINK_HLEN);
  ip->ttl = 255;
  ip->daddr = dst_ip;

  if (upkt->flags & UPKT_F_BROADCAST) {
    dst = "\xff\xff\xff\xff\xff\xff";
  } else {
    printf("Only broadcast not supported for IP\n");
    return;
  }

  ip->ihl = UPKT_IP_HLEN / 4;
  ip->check = ip_checksum(ip, sizeof(struct iphdr));

  ether_output(upkt, uif, ETH_P_IP, dst);
}
