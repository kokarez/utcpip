#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include <linux/if.h>
#include <linux/if_tun.h>

#include "dhcp.h"
#include "uiface.h"
#include "tcpip.h"
#include "upkt.h"

#define DEVTAP "/dev/net/tun"

static volatile int quit_loop = 0;

void user_handler(int useless) {
   (void)useless;
   dump_stats();
}

void int_handler(int useless) {
   (void)useless;
   quit_loop = 1;
}

void tap_output(uiface *uif, useless_pkt *upkt) {
  void *base = upkt_header(upkt, UPKT_L_ETH);

  if (write(uif->fd, base, upkt->size) == -1) {
    perror("Unable to write into this iface");
  }
}

void tcpip_setup_tap(uiface *uif) {
  struct ifreq ifr;

  uif->fd = open(DEVTAP, O_RDWR);
  if (uif->fd == -1) {
    perror("failed to open " DEVTAP);
    exit(1);
  }

  uif->output = tap_output;

  memset(&ifr, 0, sizeof(ifr));
  ifr.ifr_flags = IFF_TAP | IFF_NO_PI;
  memcpy(ifr.ifr_name, uif->name, 6);
  if (ioctl(uif->fd, TUNSETIFF, (void *) &ifr) < 0) {
    perror("tapif_init: "DEVTAP" ioctl TUNSETIFF");
    exit(1);
  }

  memset(&ifr, 0, sizeof(ifr));
  if (ioctl(uif->fd, TUNGETIFF, (void *) &ifr) < 0) {
    perror("tapif_init: "DEVTAP" ioctl TUNGETIFF");
    exit(1);
  }

  strncpy(uif->name, ifr.ifr_name, UIFACE_NAME_MAX);
  printf("Using tap named %s\n", uif->name);

  memset(&ifr, 0, sizeof(ifr));
  if (ioctl(uif->fd, SIOCGIFHWADDR, &ifr) == 0) {
    memcpy(uif->mac, ifr.ifr_hwaddr.sa_data, 6);

    printf("Tap mac: %s\n", mactostr(uif->mac));
  }

}

void tcpip_loop(uiface *uif) {
  int    result;
  fd_set readset;

  tcpip_setup_tap(uif);

  dhcp_init(uif);

  while (!quit_loop) {
    void *data = malloc(9000);

    do {
      FD_ZERO(&readset);
      FD_SET(uif->fd, &readset);

      result = select(uif->fd + 1, &readset, NULL, NULL, NULL);
    } while (result == -1 && errno == EINTR);

    if (result == -1) {
      perror("select failed");
      break;
    } else if (result == 0) {
      continue;
    }

    dhcp_update(uif);

    if (FD_ISSET(uif->fd, &readset)) {
      result = read(uif->fd, data, 9000);
      if (result > 0) {
        useless_pkt *upkt = upkt_alloc(UPKT_L_ETH, result, data);
        ether_input(upkt, uif);
      } else {
        close(uif->fd);
        perror("recv failed");
        break;
      }
    }
  }

  close(uif->fd);
  printf("Let's quit\n");
}

uiface *g_uif = NULL;
int main(int argc, char **argv) {
  uiface uif;

  /* TODO: remove this temp global */
  g_uif = &uif;

  printf("The non-working new non-powerfull tcpip is starting\n");

  signal(SIGUSR1, user_handler);
  signal(SIGINT, int_handler);

  memset(&uif, 0, sizeof (uiface));

  if (argc == 1) {
    strncpy(uif.name, "utap0", UIFACE_NAME_MAX);
  } else if (argc == 2) {
    strncpy(uif.name, argv[1], UIFACE_NAME_MAX);
  } else {
    return 0;
  }

  tcpip_loop(&uif);

  return 0;
}
